package iRepository

import (
	"gitlab.com/RubenRojas/mod-challenge/domain/model"
)

type ModerationRespositoryInterface interface {
	ModerateMessage(message model.InputMessageModel) model.ModerationResponseModel
}
