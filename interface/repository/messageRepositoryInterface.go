package iRepository

import (
	"gitlab.com/RubenRojas/mod-challenge/domain/model"
)

type MessageRespositoryInterface interface {
	SaveMessage(message model.InputMessageModel)
	GetAll() []model.SaveMessageModel
}
