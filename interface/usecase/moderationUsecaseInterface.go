package iUsecase

import (
	"gitlab.com/RubenRojas/mod-challenge/domain/model"
)

type ModerationUsecaseInterface interface {
	ModerateMessage(message model.InputMessageModel) model.ModerationResponseModel
}
