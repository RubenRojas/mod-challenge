package iUsecase

import (
	"gitlab.com/RubenRojas/mod-challenge/domain/model"
)

type MessageUsecaseInterface interface {
	GetAll() []model.SaveMessageModel
}
