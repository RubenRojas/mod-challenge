package model

import "time"

type InputMessageModel struct {
	Room   string `json:"room"`
	Text   string `json:"text"`
	UserId string `json:"user_id"`
}

type SaveMessageModel struct {
	Room   string    `json:"room" bson:"room,omitempty"`
	Text   string    `json:"text" bson:"text,omitempty"`
	UserId string    `json:"user_id" bson:"user_id,omitempty"`
	SentAt time.Time `json:"sent_at" sent_at:"email,omitempty"`
}
