package model

type StopwordModel struct {
	Token     interface{} `bson:"token"` //interface because token could be any value (string and integer)
	Exception string      `bson:"exception"`
	Risk      int         `bson:"risk"`
	Category  string      `bson:"category"`
}

type OccurrenceModel struct {
	Stopwords []StopwordModel `bson:"stopword"` //interface because token could be any value (string and integer)
	Word      string          `bson:"word"`
}
