package model

type ModerationResponseModel struct {
	Sift siftModel `json:"sift"`
}

type siftModel struct {
	Response bool     `json:"response"`
	Risk     int      `json:"risk"`
	Language string   `json:"languaje"`
	Category []string `json:"category"`
}
