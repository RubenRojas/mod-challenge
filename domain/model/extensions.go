package model

import (
	"encoding/json"
	"regexp"
	"time"
)

func (o OccurrenceModel) Validate() *StopwordModel {
	word := o.Word
	for _, ocurr := range o.Stopwords {
		if word == ocurr.Token {
			return &ocurr
		}
	}

	re, err := regexp.Compile(`[^\w]`)
	if err != nil {
		panic(err)
	}
	word = re.ReplaceAllString(word, "")

	for _, ocurr := range o.Stopwords {
		if word == ocurr.Token {
			return &ocurr
		}
	}
	return nil
}

func (s *StopwordModel) ToString() string {
	b, err := json.Marshal(s)
	if err != nil {
		panic(err)
	}
	return string(b)
}

func (m InputMessageModel) ToSave() SaveMessageModel {
	return SaveMessageModel{
		Room:   m.Room,
		Text:   m.Text,
		UserId: m.UserId,
		SentAt: time.Now(),
	}
}
