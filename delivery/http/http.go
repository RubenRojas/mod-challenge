package http

import (
	"gitlab.com/RubenRojas/mod-challenge/domain/model"
	iUsecase "gitlab.com/RubenRojas/mod-challenge/interface/usecase"

	"github.com/kataras/iris/v12"
)

type ServiceHandler struct {
	ModerationUsecase iUsecase.ModerationUsecaseInterface
	MessageUsecase    iUsecase.MessageUsecaseInterface
}

func NewStopwordHandler(r iris.Party, us iUsecase.ModerationUsecaseInterface, msgUc iUsecase.MessageUsecaseInterface) {
	handler := &ServiceHandler{ModerationUsecase: us, MessageUsecase: msgUc}
	r.Get("/", handler.Welcome)
	r.Get("/getMessages", handler.GetMessages)
	r.Post("/mod", handler.ModerateMessage)
}

func (s *ServiceHandler) GetMessages(ctx iris.Context) {
	ctx.JSON(s.MessageUsecase.GetAll())
}

func (s *ServiceHandler) Welcome(ctx iris.Context) {
	ctx.ViewData("message", "Hellow hellow!")
	ctx.View("welcome.html")
}

func (s *ServiceHandler) ModerateMessage(ctx iris.Context) {
	var message model.InputMessageModel
	var response model.ModerationResponseModel

	err := ctx.ReadJSON(&message)
	if err != nil {
		panic(err)
	}
	response = s.ModerationUsecase.ModerateMessage(message)
	ctx.JSON(response)
}
