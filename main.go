package main

import (
	"context"
	"fmt"
	"os"

	_delivery "gitlab.com/RubenRojas/mod-challenge/delivery/http"
	"gitlab.com/RubenRojas/mod-challenge/repository"
	"gitlab.com/RubenRojas/mod-challenge/usecase"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/logger"
	"github.com/kataras/iris/v12/middleware/recover"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mClient *mongo.Client

func connectMongo() {
	clientOptions := options.Client().ApplyURI(os.Getenv("MONGO_URI"))
	var err error
	mClient, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic("Error en la conexion: " + os.Getenv("MONGO_URI") + " :: " + err.Error())
	}
	fmt.Println("MONGO Successfully connected and pinged.")
	return
}

func main() {
	app := iris.New()
	app.Logger().SetLevel("debug")

	app.Use(recover.New())
	app.Use(logger.New())

	connectMongo()

	messagesCollection := mClient.Database(os.Getenv("DB_NAME")).Collection(os.Getenv("COLL_MESSAGES_NAME"))
	messageRepo := repository.NewMessageRespository(messagesCollection)
	messageUsecase := usecase.NewMessageUsecase(messageRepo)

	stopwordsCollection := mClient.Database(os.Getenv("DB_NAME")).Collection(os.Getenv("COLL_STOPWORDS_NAME"))
	modertionRepository := repository.NewModerationRespository(stopwordsCollection, messageRepo)
	moderationUsecase := usecase.NewModerationUsecase(modertionRepository)

	_delivery.NewStopwordHandler(app, moderationUsecase, messageUsecase)

	app.RegisterView(iris.HTML("./public/html", ".html"))

	app.Listen(":" + os.Getenv("APP_PORT"))

}
