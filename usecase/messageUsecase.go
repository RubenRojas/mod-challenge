package usecase

import (
	"gitlab.com/RubenRojas/mod-challenge/domain/model"
	iRepository "gitlab.com/RubenRojas/mod-challenge/interface/repository"
	iUsecase "gitlab.com/RubenRojas/mod-challenge/interface/usecase"
)

type MessageUsecase struct {
	messageRepo iRepository.MessageRespositoryInterface
}

func NewMessageUsecase(repo iRepository.MessageRespositoryInterface) iUsecase.MessageUsecaseInterface {
	return &MessageUsecase{
		messageRepo: repo,
	}
}

func (m *MessageUsecase) GetAll() []model.SaveMessageModel {
	return m.messageRepo.GetAll()
}
