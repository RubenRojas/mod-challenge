package usecase

import (
	"gitlab.com/RubenRojas/mod-challenge/domain/model"
	iRepository "gitlab.com/RubenRojas/mod-challenge/interface/repository"
	iUsecase "gitlab.com/RubenRojas/mod-challenge/interface/usecase"
)

type ModerationUsecase struct {
	moderationRepo iRepository.ModerationRespositoryInterface
}

func NewModerationUsecase(a iRepository.ModerationRespositoryInterface) iUsecase.ModerationUsecaseInterface {
	return &ModerationUsecase{
		moderationRepo: a,
	}
}

func (a *ModerationUsecase) ModerateMessage(message model.InputMessageModel) model.ModerationResponseModel {
	return a.moderationRepo.ModerateMessage(message)
}
