package repository

import (
	"context"

	iRepository "gitlab.com/RubenRojas/mod-challenge/interface/repository"

	"gitlab.com/RubenRojas/mod-challenge/domain/model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MessageRespository struct {
	coll *mongo.Collection
}

func NewMessageRespository(mCollection *mongo.Collection) iRepository.MessageRespositoryInterface {

	return &MessageRespository{
		coll: mCollection,
	}

}

func (m *MessageRespository) SaveMessage(message model.InputMessageModel) {
	messageToSave := message.ToSave()

	_, err := m.coll.InsertOne(context.TODO(), messageToSave)
	if err != nil {
		panic(err)
	}
}

func (m *MessageRespository) GetAll() []model.SaveMessageModel {
	data := []model.SaveMessageModel{}

	cursor, err := m.coll.Find(context.TODO(), bson.D{})
	if err != nil {
		panic(err)
	}

	if err := cursor.All(context.TODO(), &data); err != nil {
		panic(err)
	}

	return data
}
