package repository

import (
	"context"

	"regexp"
	"strings"

	"gitlab.com/RubenRojas/mod-challenge/domain/model"
	iRepository "gitlab.com/RubenRojas/mod-challenge/interface/repository"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ModerationRespository struct {
	coll  *mongo.Collection
	mRepo iRepository.MessageRespositoryInterface
}

func NewModerationRespository(mCollection *mongo.Collection, messageRepo iRepository.MessageRespositoryInterface) iRepository.ModerationRespositoryInterface {
	return &ModerationRespository{
		coll:  mCollection,
		mRepo: messageRepo,
	}

}

func (c *ModerationRespository) fetchByWord(word string, needsCleanText int) model.OccurrenceModel {
	var data []model.StopwordModel
	var cleanWord string

	if needsCleanText == CLEAN_TEXT {
		re, err := regexp.Compile(`[^\w]`)
		if err != nil {
			panic(err)
		}
		cleanWord = re.ReplaceAllString(word, "")
	} else {
		cleanWord = word
	}

	filter := bson.D{bson.E{Key: "token", Value: bson.D{
		{"$regex", primitive.Regex{Pattern: cleanWord, Options: "i"}},
	}}}

	opts := options.Find().SetSort(bson.D{{"risk", 1}})

	cursor, err := c.coll.Find(context.TODO(), filter, opts)
	if err != nil {
		panic(err)
	}

	if err := cursor.All(context.TODO(), &data); err != nil {
		panic(err)
	}

	if cleanWord != word {
		// en caso que la palabra tenga puntos o guion
		otherOcurrences := c.fetchByWord(word, NO_CLEAN_TEXT)
		data = append(data, otherOcurrences.Stopwords...)
	}

	return model.OccurrenceModel{
		Word:      word,
		Stopwords: data,
	}
}

func (c *ModerationRespository) ModerateMessage(message model.InputMessageModel) model.ModerationResponseModel {

	var finds []model.OccurrenceModel
	words := strings.Split(message.Text, " ")
	for _, word := range words {
		if len(word) > 1 {
			finds = append(finds, c.fetchByWord(word, CLEAN_TEXT))
		}

	}

	response := model.ModerationResponseModel{}
	// response default data
	response.Sift.Language = "es"
	response.Sift.Response = true

	for _, item := range finds {
		stopword := item.Validate()
		if stopword != nil {
			//debug.Print(stopword.ToString())
			if response.Sift.Risk < stopword.Risk {
				response.Sift.Risk = stopword.Risk
			}

			if stopword.Category != "" && !contains(response.Sift.Category, stopword.Category) {
				response.Sift.Category = append(response.Sift.Category, stopword.Category)
			}
		}
	}

	if response.Sift.Category == nil {
		response.Sift.Category = []string{""}
	}

	c.mRepo.SaveMessage(message)

	return response
}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
